﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace BrillianceTest.Controllers
{
    [Route("api/[controller]")]
    public class ArrayCalcController : Controller
    {
        #region Public Methods

        /// <summary>
        /// Funtion for reversing the array
        /// </summary>
        /// <param name="productIds">Array of Products Id</param>
        /// <returns>Returns the reversed integer array.</returns>
        [Route("reverse")]
        public int[] Reverse(int[] productIds)
        {
            //Array.Reverse<int>(productIds);
            int length = productIds.Length - 1;
            int[] reversedProductIds = new int[productIds.Length];
            int count = 0;
            while (length >= 0)
            {
                reversedProductIds[count] = productIds[length];
                length--;
                count++;
            }

            return reversedProductIds;
        }

        /// <summary>
        /// Function for deleting value at the given position
        /// </summary>
        /// <param name="productIds">Array of Products Id</param>
        /// <returns>Returns the integer array with deleted element if correct position found within it.</returns>
        [Route("deletepart")]
        public int[] DeletePart(int[] productIds)
        {
            return Array.FindAll(productIds, isNotThree);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// Check if number is 3 or not
        /// </summary>
        /// <param name="n">Gets the number</param>
        /// <returns>Returns a boolean value based on the check for 3</returns>
        static bool isNotThree(int n)
        {
            return n != 3;
        }

        #endregion
    }
}
